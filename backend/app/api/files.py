#!/usr/bin/env python

from app.config import file_types
import os

def search(type):
    if type in file_types:
        directory = file_types[type]['path']
        if type == 'wallpaper':
            if os.path.exists(os.path.join(directory, 'wallpaper.png')):
                return ['wallpaper.png']
            else:
                return []
        if os.path.isdir(directory):
            return os.listdir(directory), 200
        else:
            return [], 200
    return None, 404
