from app import utils
from connexion import NoContent

def search():
	db = utils.get_db()
	data = db['license_data'].find_one() or {}
	data['macs'] = (
		utils.get_mac_addresses()[0],
		utils.get_disk_id()
	)
	return data, 200

def post(body):
	db = utils.get_db()
	body['id'] = 1
	body.pop('macs', None)
	db.begin()
	try:
		db['license_data'].upsert(body, ['id'])
		db.commit()
	finally:
		db.rollback()

	return '', 200
