# Extended XPM plugin from / for Pillow
#
# This is currently being monkey-patched into Pillow (see bottom of file) and
# should be imported before Pillow's XPM functionality is used.
#
# *This should be moved upstream.* (obviously)
#


import re
from operator import itemgetter

from PIL import Image, ImageColor, ImageFile
from PIL._binary import o8

# XPM header
xpm_head = re.compile(br'"\s*([0-9]*)\s+([0-9]*)\s+([0-9]*)\s+([0-9]*)')

# color definition (e.g. 'c #00FFFF' or 'c dark slate blue' or 'g grey80')
color_def = re.compile(br"\s*(c|g4?|m|s)\s+(.+?)(?=$|\s+(?:c|g4?|m|s)\s)")

first = itemgetter(0)

def _accept(prefix):
    return prefix[:9] == b"/* XPM */"


##
# Image plugin for X11 pixel maps.


class XpmImageFile(ImageFile.ImageFile):

    format = "XPM"
    format_description = "X11 Pixel Map"

    def _open(self):
        if not _accept(self.fp.read(9)):
            raise SyntaxError("not an XPM file")

        # skip forward to next string
        while True:
            s = self.fp.readline()
            if not s:
                raise SyntaxError("broken XPM file")
            m = xpm_head.match(s)
            if m:
                break

        self._size = int(m.group(1)), int(m.group(2))
        self.pal = int(m.group(3))
        self.cpp = int(m.group(4))

        self.mode = "RGB"
        self.tile = [("raw", (0, 0) + self.size, self.fp.tell(), ("RGB", 0, 1))]

    def _readline(self):
        s = b'/'
        while s[0:1] == b'/':
            s = self.fp.readline().strip(b"\r\n")
        return s

    def load_read(self, bytes):
        #
        # load all image data in one chunk

        # read colors section

        palette = {}
        transparency = []

        for i in range(self.pal):
            s = self._readline()[1:-2]
            c, s = s[:self.cpp], s[self.cpp + 1:]
            if not color_def.match(s):
                raise ValueError("cannot read this XPM file")
            # Sort color candidates by preferred key (c < g < g4 < m < s).
            # Reverse, so later entries "override" earlier ones.
            # Then take the first interpretable color.
            for key, rgb in sorted(reversed(color_def.findall(s)), key = first):
                if key == b"s" or rgb == b"None":
                    transparency.append(c)
                    break
                elif rgb[0:4] in (b"gray", b"grey"):
                    n = rgb[4:]
                    if n:
                        n = int(n) * 255 // 100
                        rgb = n * (65536 + 256 + 1)
                    else:
                        rgb = 0x808080
                else:
                    if rgb[0:1] != b"#":
                        rgb = rgb.decode('ascii').replace(' ', '').lower()
                        rgb = ImageColor.colormap.get(rgb)
                        if rgb is None:
                            continue
                    rgb = int(rgb[1:], 16)

                palette[c] = (
                    o8((rgb >> 16) & 255) + o8((rgb >> 8) & 255) + o8(rgb & 255)
                )
                break
            else:
                # fallback color: black
                palette[c] = b'\0\0\0'

        # find unused color for transparency
        if transparency:
            rgb = 1
            while rgb.to_bytes(3, 'big') in palette.values():
                rgb += 1
            rgb = rgb.to_bytes(3, 'big')
            for c in transparency:
                palette[c] = rgb
            self.info["transparency"] = rgb

        # read pixels section

        xsize, ysize = self.size

        s = []
        r = re.compile(b"." * self.cpp)

        for i in range(ysize):
            l = self._readline()
            l = l[1 : (xsize + 1) * self.cpp]
            s.extend(map(palette.get, r.findall(l)))

        return b"".join(s)


#
# Registry

# dirty hack: ensure all plugins are loaded, so we can overwrite XpmImagePlugin
Image.init()

Image.register_open(XpmImageFile.format, XpmImageFile, _accept)

Image.register_extension(XpmImageFile.format, ".xpm")

Image.register_mime(XpmImageFile.format, "image/xpm")
