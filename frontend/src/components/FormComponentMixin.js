var $_uid = 0
export default {
  props: {
    name: String,
    value: String,
    disabled: {
      type: Boolean,
      default: false
    },
    schema: {
      type: Object,
      default: Object
    },
    i18n_path: String,
    level: Number
  },
  computed: {
    valid() {
      return true
    },
    uid() {
      return $_uid++;
    }
  },
  methods: {
    validate(){},
    focus(last = false) { this.$emit(last? "focus_prev" : "focus_next") },
    focus_first() { this.focus() },
    focus_last() { this.focus(true) },
    focus_prev() { this.$emit("focus_prev") },
    focus_next() { this.$emit("focus_next") }
  }
}
