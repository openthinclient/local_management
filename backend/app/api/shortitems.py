#!/usr/bin/env python

from app import utils
from flask import current_app


def search(type='', subtype='', name='', mac_address=''):
    db = utils.get_db()
    table_shortitems = db['shortitems']

    if mac_address:
        related_item_ids = list()
        client_object = table_shortitems.find_one(mac=mac_address)
        client_id = client_object['id']
        related_item_ids.append(client_id)
        related_items, http_code = app.api.relationships.search(id=client_id, flat=True)
        for related_item in related_items:
            related_item_ids.append(related_item['id'])

    filter = dict()
    if type:
        filter['type'] = type
    if subtype:
        filter['subtype'] = subtype
    if name:
        filter['name'] = name
    if mac_address:
        filter['id'] = related_item_ids

    shortitems = table_shortitems.all(**filter)

    result = list(shortitems)

    current_app.logger.info('Item filter type: "{type}", subtype: "{subtype}", name="{name}", mac="{mac_address}", number of results: {results}'
                            .format(type=type, subtype=subtype, name=name, mac_address=mac_address, results=len(result)))

    return result, 200
