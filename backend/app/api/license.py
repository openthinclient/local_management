from app import utils
from connexion import NoContent
from flask import current_app
import requests
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto.Util.Padding import unpad
from dateutil.parser import isoparse
from base64 import b64decode
import json
from datetime import datetime, timezone

EVALUATION_DAYS = 31
GRACE_PERIOD_DAYS = 30

LICENSE_URL = 'http://license.openthinclient.com/v1/get_stick_license'

PUBLIC_KEY = RSA.import_key(b64decode(b'''
	MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr9inZYrg5lcN
	qXgZ0d7yM0OYVR5HoopeidQ00qFRU8JmesWwKtpM12o+SJlSro+ZKwjY
	q5YQvNNSNgjAJxvGlcge4lhAbnKrDVMUyNUWNf3jDqO+yiEHn+7gIo1z
	eI1iHuXC4+Zt7sV7o53A2hxjY1/+eN/cfYLVknilQJ9dNHvyJkoLg9VK
	/nKNe4IGOuf1e4Ta8uYMgKWIA5ZZw/7ZpBYASdeeQszy2iPs7YRccCb2
	Tblm47W5jytFbaFbzeSSK71K2bIPMFKRVSg7KAcBVHzjgJ5E2M1ckene
	pFNasrh0WA/FcpzZEnRxqByDpQdlnzvYTujCqpUo+W6fIQmpDQIDAQAB
'''))

LICENSE_DEFAULTS = {
	'customerID': None,			# from encrypted license (or None)
	'expirationDate': None,		# from encrypted license (or None)
	'email': None,				# from encrypted license (or None)
	'mac': None,				# from encrypted license (or None)
	'valid': True,				# license valid, within evaluation or grace period
	'type': 'full',				# 'evaluation' or 'full'
	'grace_period': False,		# expired license within grace period
	'remaining_days': 0 		# days left of license, evaluation or grace period
}

def search():
	today = datetime.now().date()

	license = _get_license()
	if license:
		license['type'] = 'full'
		license['expirationDate'] = _parse_datetime(license['expirationDate'])

		expiration_date = license['expirationDate'].date()
		if today <= expiration_date:
			license['remaining_days'] = (expiration_date - today).days
		else:
			days_past_expiration = (today - expiration_date).days
			if days_past_expiration <= GRACE_PERIOD_DAYS:
				license['grace_period'] = True
				license['remaining_days'] = GRACE_PERIOD_DAYS - days_past_expiration
			else:
				license['valid'] = False
	else:
		firstboot_date = _get_firstboot_date()
		if firstboot_date:
			license['type'] = 'evaluation'
			usage_days = (today - firstboot_date).days
			if usage_days > EVALUATION_DAYS:
				license['valid'] = False
			else:
				license['remaining_days'] = EVALUATION_DAYS - usage_days

	result = LICENSE_DEFAULTS.copy()
	result.update(license)
	return result, 200


def _get_license():
	db = utils.get_db()
	encrypted_license = db['license'].find_one()

	if not encrypted_license:
		return {}

	try:
		license = decrypt_license(encrypted_license)
	except Exception as ex:
		current_app.logger.exception('Failed to decrypt license.')
		return {}

	valid_ids = (*utils.get_mac_addresses(), utils.get_disk_id())
	if license['mac'].upper() in valid_ids:
		return license
	else:
		return {}


def _get_firstboot_date():
	firstboot = utils.get_boot_arguments().get('tcos.firstboot')
	if not firstboot:
		current_app.logger.warning('Kernel cmdline parameter tcos.firstboot not set. Did somebody try to manipulate it?')
		return None

	try:
		return datetime.strptime(firstboot, '%Y-%m-%d').date()
	except ValueError as e:
		current_app.logger.warning('Kernel cmdline parameter tcos.firstboot has wrong format. Did somebody try to manipulate it?')
		return None


def post(encrypted_license=None):
	db = utils.get_db()

	if not encrypted_license:
		try:
			encrypted_license = download_encrypted_license()
		except requests.Timeout as ex:
			return _exception_json(ex), 504
		except (requests.RequestException, json.JSONDecodeError) as ex:
			if isinstance(ex, requests.HTTPError) and ex.response.status_code == 404:
				return {
					"status_code": ex.response.status_code,
					"reason": ex.response.reason,
					"content": ex.response.content.decode('utf-8'),
					**_exception_json(ex)
				}, 404
			return _exception_json(ex), 502

		if not encrypted_license:
			return {}, 200

	encrypted_license['id'] = 1

	db.begin()
	try:
		db['license'].upsert(encrypted_license, ['id'])
		db.commit()
	except Exception as ex:
		raise ex
		db.rollback()

	license = decrypt_license(encrypted_license)
	license['expirationDate'] = _parse_datetime(license['expirationDate'])
	return license, 200


def _exception_json(ex):
	# args = [isinstance(arg, Exception) and _exception_json(arg) or arg for arg in ex.args[:]]
	args = map(lambda arg: isinstance(arg, Exception) and _exception_json(arg) or arg, ex.args[:])
	return {
		"type": ex.__class__.__name__,
		"args": [*args]
	}


def _parse_datetime(date_string):
	return isoparse(date_string).astimezone().replace(tzinfo=None)


def decrypt_license(encrypted_license):
	license = b64decode(encrypted_license['license'])
	encryption_key = b64decode(encrypted_license['encryptionKey'])
	aes_key = b64decode(simple_RSA_decrypt(encryption_key))
	padded_license = AES.new(aes_key, AES.MODE_ECB).decrypt(license)
	return json.loads(unpad(padded_license, 16).decode('utf-8'))


def simple_RSA_decrypt(ciphertext):
	# Since we are using RSA "backwards" we can not use the blinded decrypt
	# of the library (as this requires access to p and q). Instead we simply
	# encrypt with the public key (which is essentially the same as
	# decryption) and then remove the PKCS padding.
	cipher = int.from_bytes(ciphertext, 'big')
	padded_message = PUBLIC_KEY._encrypt(cipher).to_bytes(255, 'big')
	return padded_message.split(b'\0', 1)[1]


def download_encrypted_license():
	db = utils.get_db()
	license_data = db['license_data'].find_one()
	if not license_data:
		return {}

	if license_data.get('mac_source') == 1:
		license_data['mac'] = utils.get_disk_id()
	else:
		license_data['mac'] = utils.get_mac_addresses()[0]

	keys = ('pin', 'mac', 'email', 'customer_id')

	if not all(license_data.get(key) for key in keys):
		return {}

	params = {key: license_data[key] for key in keys}

	current_app.logger.info('Downloading license')
	response = requests.get(LICENSE_URL, params = params)
	if response.status_code != 200:
		response.raise_for_status()
	return json.loads(response.content.decode('utf-8'))
