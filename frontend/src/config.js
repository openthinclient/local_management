const LOCAL_MANAGEMENT = true
const REST_BASE = `http://${location.hostname}:5000/v1/`
const FILE_TYPES = {
  icon: {
    accept: [
      '.xpm',
      'image/png',
      'image/svg',
      'image/svg+xml',
      'image/x-xpmi',
      'image/x-xpixmap'
    ]
  },
  certificate: {
    accept: [
      'application/x-x509-ca-cert',
      'application/pkix-cert',
      '.der',
      '.crt',
      '.cer'
    ]
  },
  wallpaper: {
    accept: [
      'image/*'
    ]
  },
  openvpn: {
    accept: [
      '.ovpn',
      'application/x-openvpn-profile'
    ]
  }
}
const PATH_TO_TYPE = {
  'schema.application.openvpn.Application.configFile': 'openvpn'
}

export {
  LOCAL_MANAGEMENT,
  REST_BASE,
  FILE_TYPES,
  PATH_TO_TYPE
}
