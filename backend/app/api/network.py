#!/usr/bin/env python

from app import utils
import app.api.schema
from flask import current_app
from connexion import NoContent
import json
import re
import configparser
import subprocess
import tempfile
import time
import os


def search():
	command = 'ip -j addr show'
	attempts = 6
	for trial in range(attempts):
		ip_output, exit_code = utils.run_command(command)
		ip_output = '\n'.join(ip_output)
		if ip_output.strip():
			break
		time.sleep(0.5)

	if exit_code != 0:
		error = {
			'code': exit_code,
			'message': {
				'de': '"{command}" ist mit folgender Meldung gescheitert: {ip_output}'.format(**locals()),
				'en': '"{command}" failed with the following result: {ip_output}'.format(**locals()),
			}
		}
		return error, 500
	try:
		interfaces = json.loads(ip_output)
	except json.decoder.JSONDecodeError as e:
		error = {
			'code': 500,
			'message': {
				'de': 'JSON Decoder ist gescheitert: {e}'.format(**locals()),
				'en': 'JSON Decoder failed: {e}'.format(**locals()),
			}
		}
		return error, 500


	config = dict()
	for interface in interfaces:
		name = interface["ifname"]
		command = 'networkctl status {name}'.format(**locals())
		vendor = model = network_file = state = gateway = ip = netmask = type = None
		dns = list()
		networkctl_output, exit_code = utils.run_command(command)
		networkctl_output = '\n'.join(networkctl_output)
		if exit_code != 0:
			error = {
				'code': exit_code,
				'message': {
					'de': '"{command}" ist mit folgender Meldung gescheitert: {networkctl_output}'.format(**locals()),
					'en': '"{command}" failed with the following result: {networkctl_output}'.format(**locals()),
				}
			}
			return error, 500

		# Get Vendor
		try:
			vendor = re.search(r' *Vendor: (.*)', networkctl_output).group(1)
		except AttributeError:
			vendor = None
		# Get Model
		try:
			model = re.search(r' *Model: (.*)', networkctl_output).group(1)
		except AttributeError:
			model = None
		# Get state of interface
		try:
			operstate, setupstate = re.search(r' *State: (.*) \((.*)\)', networkctl_output).groups()
			if operstate in ('missing', 'enslaved') \
					or setupstate in ('pending', 'linger'):
				continue
		except AttributeError:
			operstate = setupstate = None
		# network systemd unit file path
		# (networkctl gives 'Network File: n/a' for unmanaged devices, so we always use default path)
		network_file = '/etc/systemd/network/50-' + name + '.network'

		config_file = configparser.ConfigParser(strict=False)
		try:
			config_file.read(network_file)
		except (configparser.MissingSectionHeaderError, configparser.ParsingError) as e:
			error = {
				'code': 1,
				'message': {
					'de': 'Kann {network_file} nicht parsen: {e}'.format(**locals()),
					'en': 'Failed to parse {network_file}: {e}'.format(**locals()),
				}
			}
			return error, 500

		# Get DHCP Status
		dhcp = (config_file.has_option('Network', 'DHCP') and config_file['Network']['DHCP'] != 'no')

		if dhcp:
			# Get ip/netmask from ip output
			for address in interface['addr_info']:
				if address['family'] == 'inet':
					ip = address['local']
					netmask = utils.prefixlen_to_netmask(address['prefixlen'])
			try:
				gateway = re.search(r' *Gateway: ([\d.]*)', networkctl_output).group(1)
			except AttributeError:
				gateway = None
			try:
				dns_raw = re.search(r'^ *DNS: ([\d.]*)(?:\s+([\d.]*))?', networkctl_output, re.MULTILINE).groups()
				dns = [ip for ip in dns_raw if utils.is_valid_ipv4_address(ip)]
			except AttributeError:
				dns = list()
		else:
			# Get Config from network_file
			try:
				cidr = config_file['Network']['Address'].strip()
			except KeyError:
				cidr = None
			if cidr:
				ip, netmask = utils.cidr_to_subnetting(cidr)
				if not utils.is_valid_ipv4_address(ip):
					ip = None
			try:
				gateway = config_file['Network']['Gateway'].strip()
				if not utils.is_valid_ipv4_address(gateway):
					gateway = None
			except KeyError:
				gateway = None
			try:
				dns_raw = config_file['Network']['DNS'].strip()
				dns = dns_raw.split()
			except KeyError:
				dns = list()


		if name.startswith('en') or name.startswith('eth'):
			type = 'ethernet'
		elif name.startswith('wl'):
			type = 'wifi'
		else:
			continue

		try:
			mac = interface['address']
		except KeyError:
			mac = '00:00:00:00:00:00'

		if not os.path.exists(network_file):
			# Default to DHCP for new interfaces
			dhcp = True

		config[name] = {
			'vendor': vendor,
			'model': model,
			'operstate': operstate,
			'setupstate': setupstate,
			'dhcp': dhcp,
			'type': type,
			'mac': mac,
			'ip': ip,
			'netmask': netmask,
			'dns': dns,
			'gateway': gateway
		}

		if type == 'wifi':
			command = 'iwgetid {} --raw'.format(name)
			config[name]['connected_essid'] = subprocess.run(
					command, shell=True, stdout=subprocess.PIPE
				).stdout.decode('utf-8').strip()

	return config, 200


def put(body):
	for interface, settings in body.items():
		if settings.get('type', '') not in ('ethernet', 'wifi'):
			current_app.logger.info('Skipping Interface {interface}'.format(**locals()))
			continue
		current_app.logger.info('Configuring Interface {interface}'.format(**locals()))
		config = configparser.ConfigParser()
		config.optionxform = str

		# Match section
		config.add_section('Match')
		config.set('Match', 'Name', interface)

		# Link section
		if settings.get('state') in ('off', 'unmanaged'):
			config.add_section('Link')
			config.set('Link', 'Unmanaged', 'yes')

		# Network section
		config.add_section('Network')
		if settings.get('dhcp', True):
			config.set('Network', 'DHCP', 'ipv4')
			config.add_section('DHCP')
			config.set('DHCP', 'SendHostname', 'true')
			config.set('DHCP', 'UseHostname', 'true')
			config.set('DHCP', 'UseDomains', 'true')
			config.set('DHCP', 'ClientIdentifier', 'mac')
			config.set('DHCP', 'VendorClassIdentifier', 'openthinclient')
		else:
			ip = settings.get('ip', None)
			if not utils.is_valid_ipv4_address(ip):
				current_app.logger.error('IP address invalid: {ip}'.format(**locals()))
				error = {
					'code': 1,
					'message': {
						'de': 'IP Adresse ungültig: {ip}'.format(**locals()),
						'en': 'IP address invalid: {ip}'.format(**locals())
					}
				}
				return error, 500

			# Address
			netmask = settings.get('netmask', None)
			if not netmask:
				current_app.logger.error('Netmask not set.'.format(**locals()))
				error = {
					'code': 1,
					'message': {
						'de': 'Netmask nicht gesetzt.',
						'en': 'Netmask not set.'
					}
				}
				return error, 500
			config.set('Network', 'Address', utils.subnetting_to_cidr(ip, netmask))

			# DNS
			dns = [entry for entry in settings.get('dns', list()) if entry and entry.strip()]
			if dns:
				for nameserver in dns:
					nameserver = nameserver.strip()
					if not utils.is_valid_ipv4_address(nameserver):
						current_app.logger.error('DNS server IP address invalid: {nameserver}'.format(**locals()))
						error = {
							'code': 1,
							'message': {
								'de': 'DNS Server IP Adresse ungültig: {nameserver}'.format(**locals()),
								'en': 'DNS server IP address invalid: {nameserver}'.format(**locals())
							}
						}
						return error, 500
				config.set('Network', 'DNS', ' '.join(dns))

			# Gateway
			gateway = (settings.get('gateway') or '').strip()
			if gateway:
				if not utils.is_valid_ipv4_address(gateway):
					current_app.logger.error('Gateway IP address invalid: {gateway}'.format(**locals()))
					error = {
						'code': 1,
						'message': {
							'de': 'Gateway IP Adresse ungültig: {gateway}'.format(**locals()),
							'en': 'Gateway IP address invalid: {gateway}'.format(**locals())
						}
					}
					return error, 500

				config.set('Network', 'Gateway', gateway)

		network_file_path = os.path.join('/config/network', '50-' + interface + '.network')

		# Backup current config to /tmp
		temp_dir = tempfile.mkdtemp(prefix='network', dir='/tmp')
		current_app.logger.info('Deleting {network_file_path}. Temporary backup in {temp_dir}.'.format(**locals()))
		utils.run_command('mv {network_file_path} {temp_dir}'.format(**locals()), hide_error=True)

		# Write config
		utils.run_command('mkdir -p /config/network')
		with open(network_file_path, 'w') as network_file:
			config.write(network_file)

	utils.run_command('rm /etc/systemd/network/50* ', hide_error=True)
	try:
		utils.run_command('cp /config/network/* /etc/systemd/network/', raise_exception=True)
	except subprocess.CalledProcessError as e:
		current_app.logger.error('Failed to copy network config to systemd folder: {e}'.format(**locals()))
		error = {
			'code': 1,
			'message': {
				'de': 'Konnte Netzwerk-Einstellungen nicht aktivieren. Bitte starten Sie den Thinclient neu. ({e})'.format(**locals()),
				'en': 'Could not activate network settings. Please reboot. ({e})'.format(**locals())
			}
		}
		return error, 500

	try:
		utils.run_command('systemctl restart systemd-networkd', raise_exception=True)
	except subprocess.CalledProcessError as e:
		current_app.logger.error('Systemd-networkd failed.'.format(**locals()))
		error = {
			'code': 1,
			'message': {
				'de': 'Systemd-networkd ist gescheitert.',
				'en': 'Systemd-networkd failed.'
			}
		}
		return error, 500

	# Shut down interfaces that not or no longer in use
	for interface, settings in body.items():
		if settings.get('state') in ('off', 'unmanaged'):
			if settings.get('type') == 'wifi':
				utils.run_command('tcos-configure-wpa -r if_down {}'.format(interface))
			utils.run_command('ip link set {interface} down'.format(**locals()))
		elif settings.get('type') == 'wifi':
			utils.run_command('tcos-configure-wpa -r if_up {}'.format(interface))

	try:
		utils.run_command('systemctl restart systemd-resolved', raise_exception=True)
	except subprocess.CalledProcessError as e:
		current_app.logger.error('Systemd-resolved failed.'.format(**locals()))
		error = {
			'code': 1,
			'message': {
				'de': 'Systemd-resolved ist gescheitert.',
				'en': 'Systemd-resolved failed.'
			}
		}
		return error, 500

	# Reload IP Applet
	utils.run_command('sudo runuser -l $(getent passwd "1000" | cut -d: -f1) -c "DISPLAY=:0 /opt/tcos-libs/tcos/update_ip_applet.sh"')

	return NoContent, 204


def delete():
	temp_dir = tempfile.mkdtemp(prefix='network', dir='/tmp')
	# Backup current config to /tmp
	current_app.logger.info('Deleting configuration. Temporary backup in {temp_dir}.'.format(**locals()))
	utils.run_command('mv /config/network/50* ' + temp_dir, hide_error=True)
	utils.run_command('rm /etc/systemd/network/50* ', hide_error=True)

	return NoContent, 204
