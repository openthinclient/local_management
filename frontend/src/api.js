/*
  Facade for server API

  * i18n messages are merged into the vue-i18n plugin with the path
    `schema.[type].[subtype]`
  * relationships are grouped by type
  * when saving relationships additional ids may be specified

*/
import {REST_BASE} from './config.js'

class WrappedError extends Error {
  constructor(message_or_cause) {
    super(message_or_cause)
    this.name = this.constructor.name
    this.cause = message_or_cause
  }
}

class ServerError extends WrappedError {}
class NotFoundError extends ServerError {}
class ServerErrorWithMessage extends ServerError {
  constructor(message_object, ...args) {
    super(...args)
    this.message_object = message_object
  }
}
class JSONError extends WrappedError {}

async function server(url, request) {
  let response

  try {
    response = await fetch(url, request)
  } catch(ex) {
    throw new ServerError(ex)
  }
  if(response.status == 200) {
    try {
      return await response.json()
    } catch(ex) {
      throw new JSONError(ex)
    }
  } else if(response.status == 204) {
    return
  } else if(response.status == 404) {
    throw new NotFoundError
  } else {
    let json
    try {
      json = await response.json()
    } catch(ex) {
      throw new ServerError(response.status)
    }
    throw new ServerErrorWithMessage(json, response.status)
  }
}

function group_relationships(json) {
  let relationships = Object.create(null)

  for(let related_shortitem of json) {
    let {type, relationships: nested_relationships} = related_shortitem

    if(!(type in relationships)) {
      relationships[type] = []
    }

    relationships[type].push(related_shortitem)

    if(nested_relationships) {
      related_shortitem.relationships = group_relationships(nested_relationships)
    }
  }
  return relationships
}

function assert_JSON_is_Array(json) {
  if(!Array.isArray(json)) {
    let type = (json == null)? json : json.constructor.name
    throw new JSONError(`expected Array, got ${type}`)
  }
}

const hasOwnProperty = Object.hasOwnProperty.call.bind(Object.hasOwnProperty)
const isSimpleObject = o =>
  o != null && [null, Object.prototype].includes(Object.getPrototypeOf(o))

// return node from keys, absent entries are initialized with an empty object
function subtree(tree, key, ...keys) {
  tree = hasOwnProperty(tree, key) ? tree[key] : tree[key] = {}
  return keys.length == 0 ? tree : subtree(tree, ...keys)
}

// expand all dotted keys so they are addressable with Vue I18n
function normalize_i18n_keys(messages) {
  for(let [key, nested_messages] of Object.entries(messages)) {
    if(isSimpleObject(nested_messages)) {
      if(key.includes('.')) {
        delete messages[key]
        Object.assign(subtree(messages, ...key.split('.')), nested_messages)
      }
      normalize_i18n_keys(nested_messages)
    }
  }
}

export default {
  ServerError,
  NotFoundError,
  ServerErrorWithMessage,
  JSONError,

  async load_schemas($i18n) {
    let json  = await server(REST_BASE + 'schema')
    assert_JSON_is_Array(json)

    let schemas = Object.create(null)
    for(let {type, subtype, schema, i18n} of json) {
      if(subtype) {
        if(!(type in schemas)) {
          schemas[type] = {}
        }
        schemas[type][subtype] = schema
      } else {
        schemas[type] = schema
      }
      for(let [locale, messages] of Object.entries(i18n)) {
        normalize_i18n_keys(messages)
        if(subtype) {
          messages = {[subtype]: messages}
        }
        $i18n.mergeLocaleMessage(locale, {schema: {[type]: messages}})
        if(type == 'hardwaretype') {
          $i18n.mergeLocaleMessage(locale, {
            client_form: {
              hardwaretype: Object.assign(
                {},
                messages,
                $i18n.t('client_form.hardwaretype_override', locale)
              )
            }
          })
        }
      }
    }
    return schemas
  },

  async load_shortitems(type, subtype) {
    let url = new URL('shortitems', REST_BASE)
    if(type != null) url.searchParams.set('type', type)
    if(subtype != null) url.searchParams.set('subtype', subtype)
    let json = await server(url)
    assert_JSON_is_Array(json)
    return json
  },

  load_item(id) {
    let item_url = new URL('item', REST_BASE)
    item_url.searchParams.set('id', id)
    return server(item_url)
  },

  async save_item(item) {
    let url = new URL('item', REST_BASE)
    if(item.id) {
      url.searchParams.set('id', item.id)
    }
    let request = {
      method: item.id? 'PUT' : 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(item)
    }

    item.id = (await server(url, request)).id
  },

  delete_item(id) {
    let url = new URL('item', REST_BASE)
    url.searchParams.set('id', id)
    let request = {
      method: 'DELETE'
    }
    return server(url, request)
  },

  async load_relationships(id) {
    let relationships_url = new URL('relationships', REST_BASE)
    relationships_url.searchParams.set('id', id)
    let json = await server(relationships_url)
    assert_JSON_is_Array(json)
    return group_relationships(json)
  },

  async set_relationships(id, relationships, ...ids) {
    let url = new URL('relationships', REST_BASE)
    url.searchParams.set('id', id)

    let relationship_ids = new Set(ids)
    for({id} of Object.values(relationships).flat()) {
      relationship_ids.add(id)
    }

    let request = {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify([...relationship_ids].map(id => ({id})))
    }

    let json = await server(url, request)
    assert_JSON_is_Array(json)
    return group_relationships(json)
  },

  async get_files(type) {
    let url = new URL('files', REST_BASE)
    url.searchParams.set('type', type)
    let json = await server(url)
    assert_JSON_is_Array(json)
    return json
  },

  upload_file(type, name, content) {
    let url = new URL('file', REST_BASE)
    url.searchParams.set('type', type)
    url.searchParams.set('name', name)
    let request = {
      method: 'PUT',
      body: content
    }
    return server(url, request)
  },

  delete_file(type, name) {
    let url = new URL('file', REST_BASE)
    url.searchParams.set('type', type)
    url.searchParams.set('name', name)
    return server(url, {method: 'DELETE'})
  },

  load_network() {
    return server(REST_BASE + 'network')
  },

  save_network(config) {
    let url = new URL('network', REST_BASE)
    let request = {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(config)
    }
    return server(url, request)
  },

  scan_wifi(ifname) {
    let url = new URL('wifi_connection', REST_BASE)
    url.searchParams.set('ifname', ifname)
    return server(url)
  },

  configure_wifi() {
    let url = new URL('command/tcos-configure-wpa', REST_BASE)
    let request = {
      method: 'POST'
    }
    return server(url, request)
  },

  get_wifi_status() {
    let url = new URL('wifi_status', REST_BASE)
    return server(url)
  },

  load_license_data() {
    return server(REST_BASE + 'license_data')
  },

  save_license_data(license_data) {
    let url = new URL('license_data', REST_BASE)
    let request = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(license_data)
    }
    return server(url, request)
  },

  async load_license() {
    let license = await server(REST_BASE + 'license')
    if(license['expirationDate']) {
      license['expirationDate'] = new Date(license['expirationDate'])
    }
    return license
  },

  update_license() {
    return server(REST_BASE + 'license', {method: 'POST', body: null})
  },

  load_xrandr() {
    return server(REST_BASE + 'xrandr')
  },

  run_xrandr(command) {
    let url = new URL('xrandr', REST_BASE)
    let request = {
      method: 'POST',
      body: command
    }
    return server(url, request)
  },

  desktop_reload() {
    let url = new URL('command/desktop-reload', REST_BASE)
    let request = {
      method: 'POST',
    }
    return server(url, request)
  }
}
