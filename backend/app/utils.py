#!/usr/bin/env python

from collections import OrderedDict, defaultdict
from flask import current_app, g
from lxml import etree
import dataset
import textwrap
import subprocess
import sys
import struct
import socket
import ipaddress
import json

# returns new database connection, if there is none
def get_db():
    # keep database in app context for in memory databases
    if ':memory:' in current_app.config['DATABASE_URI']:
        if 'db' not in current_app.config:
            current_app.config['db'] = dataset.connect(current_app.config['DATABASE_URI'])
            current_app.logger.debug('Connecting to: {}'.format(current_app.config['DATABASE_URI']))
        return current_app.config['db']

    # keep database in request context for persistent databases
    else:
        if 'db' not in g:
            g.db = dataset.connect(current_app.config['DATABASE_URI'])
            current_app.logger.debug('Connecting to: {}'.format(current_app.config['DATABASE_URI']))
        return g.db


# creates database, if not yet existent
def init_db():
    current_app.logger.info('Initializing Database...')
    db = get_db()
    db.begin()
    db.commit()


# reursively create json schema from xml
def _create_json_schema(root):
    json_schema = OrderedDict()
    for child in root.iterchildren():
        if child.tag in ['group', 'section']:
            name = child.attrib['name']
            if name in json_schema:
                json_schema[name]['properties'].update(_create_json_schema(child))
            else:
                json_schema[name] = OrderedDict(properties=_create_json_schema(child), type='object')
        elif child.tag in ['entry']:
            json_schema[child.attrib['name']] = OrderedDict(default=child.get('value', None), type='string')
            validators = [dict(validator.attrib) for validator in child.iterchildren('validator')]
            if validators:
                json_schema[child.attrib['name']]['validators'] = validators
        elif child.tag in ['password']:
            json_schema[child.attrib['name']] = OrderedDict(default=child.get('value', None), type='string', format='password')
        elif child.tag in ['choice']:
            enum = list()
            for option in child.iterchildren('option'):
                enum.append(option.attrib['value'])
            json_schema[child.attrib['name']] = OrderedDict(default=child.get('value', None), type='string', enum=enum)

    return json_schema


# reursively create i18n schema from xml
def _create_i18n(root, language):
    i18n_schema = OrderedDict()
    for child in root.iterchildren():
        # set i18n for group and section
        if child.tag in ['group', 'section']:
            messages = {**_read_i18n_common(child, language),
                        **_create_i18n(child, language)}
            name = child.attrib['name']
            if name in i18n_schema:
                i18n_schema[name].update(messages)
            else:
                i18n_schema[name] = messages
        # set i18n for simple text fields
        elif child.tag in ['entry', 'password']:
            validator_labels = list()

            for validator in child.iterchildren('validator'):
                validator_label = ''
                for label in validator.iterchildren('label'):
                    if label.attrib['lang'] == language:
                        validator_label = label.attrib['value']
                validator_labels.append(validator_label)

            i18n_schema[child.attrib['name']] = {**_read_i18n_common(child, language),
                                                'validator_labels': validator_labels}
        # set i18n information for enum choices
        elif child.tag in ['choice']:
            enum_labels = list()

            for option in child.iterchildren('option'):
                enum_label = None
                # set enum label
                for label in option.iterchildren('label'):
                    if label.attrib['lang'] == language:
                        enum_label = label.attrib['value']
                # set option value as enum label if no label was found
                if not enum_label:
                    enum_label = option.attrib.get('name', option.attrib['value'])
                enum_labels.append(enum_label)

            i18n_schema[child.attrib['name']] = {**_read_i18n_common(child, language),
                                                'enum_labels': enum_labels}

    return i18n_schema

# read common i18n data from a node
def _read_i18n_common(node, language):
    hint_text = None
    label_text = None
    article_key = node.attrib.get('kbarticle', None)
    for label in node.iterchildren('label'):
        if label.attrib['lang'] == language:
            label_text = label.attrib['value']
    for hint in node.iterchildren('tip'):
        if hint.attrib['lang'] == language:
            hint_text = hint.attrib.get('value', None)
    return {
        'label': label_text,
        'hint': hint_text,
        'article': article_key
    }


# load xml files to memory and convert them into json
def load_xmls():
    current_app.logger.info('Loading XMLs...')
    xml_files = list()
    import os
    for root, directories, filenames in os.walk(current_app.config['XML_PATH']):
        for filename in filenames:
            if os.path.splitext(filename)[-1] == '.xml':
                xml_files.append(os.path.join(root, filename))

    # load all xml files
    json_schemas = OrderedDict()
    for xml_file in xml_files:
        # parse XML
        tree = etree.parse(xml_file)
        root = tree.getroot()
        schema_name = root.attrib['name']

        current_app.logger.info(xml_file)
        # get type and subtype of current schema
        schema_type = xml_file.split('/')[-2]
        if schema_type == 'schema':
            schema_type = schema_name
            schema_subtype = None
        else:
            schema_subtype = schema_name

        # create JSON schema
        json_schemas[schema_name] = OrderedDict(type=schema_type, subtype=schema_subtype, schema=_create_json_schema(root))

        # create i18n
        json_schemas[schema_name]['i18n'] = OrderedDict()
        for language in ['de', 'en']:
            json_schemas[schema_name]['i18n'][language] = {**_read_i18n_common(root, language),
                                                           **_create_i18n(root, language)}

    current_app.json_schemas = json_schemas

    current_app.legacy_boolean = dict()
    # TODO activate legacy handling
    for name, json_schema in json_schemas.items():
        legacy_boolean_handling(json_schema)


# flatten dict
def flatten_dict(d):
    def items():
        for key, value in d.items():
            key = key.replace('.', '/')
            if isinstance(value, dict):
                for subkey, subvalue in flatten_dict(value).items():
                    yield key + "/" + subkey, subvalue
            else:
                yield key, value

    return dict(items())


def deflatten_dict(d):
    def tree():
        return defaultdict(tree)

    def get_tree_node(subtree, keys):
        if not keys:
            return subtree
        if len(keys) == 1:
            return subtree[keys[0]]
        else:
            return get_tree_node(subtree[keys.pop(0)], keys)

    out = tree()
    for key, val in d.items():
        *keys, key = key.split('/')
        get_tree_node(out, keys)[key] = val
    return out


def get_defaults(json_schema):
    default_settings = dict()
    schema = flatten_dict(json_schema['schema'])
    # Parse JSON schema
    for key, value in schema.items():
        # TODO implement integer/float defaults?
        if key.endswith('type') and value == 'string':
            default_key = key[:-4] + 'default'
            default_value = schema.get(default_key, '')
            if default_value is None:
                default_value = ''
            default_settings[key.replace('/properties/', '/')[:-5]] = default_value
        if key.endswith('type') and value == 'boolean':
            default_key = key[:-4] + 'default'
            default_value = schema.get(default_key, False)
            if default_value is None:
                default_value = False
            default_settings[key.replace('/properties/', '/')[:-5]] = default_value

    return default_settings


def check_legacy_boolean(enum):
    """ checks a list of strings if it is a legacy boolean """
    true_values = ['yes', 'ja', 'on', 'an', 'true']
    false_values = ['no', 'nein', 'off', 'aus', 'false']

    if len(enum) == 2 \
            and (enum[0].lower() in true_values or enum[0].lower() in false_values) \
            and (enum[1].lower() in true_values or enum[1].lower() in false_values):
        return True
    else:
        return False


def translate_legacy_boolean_to_boolean(value):
    """ translate legacy boolean string in real boolean value """
    true_values = ['yes', 'ja', 'on', 'an', 'true']
    false_values = ['no', 'nein', 'off', 'aus', 'false']

    if value.lower() in true_values:
        return True
    elif value.lower() in false_values:
        return False


def legacy_boolean_handling(json_schema, path=None):
    """
    Replacing legacy booleans in JSON schema with real boolean values and saving legacy boolean strings in
    current_app.legacy_boolean.
    """

    # Setup some variables in first recursion
    if path is None:
        type = '' if json_schema['type'] is None else json_schema['type']
        subtype = '' if json_schema['subtype'] is None else json_schema['subtype']
        path = [type, subtype]
        legacy_boolean_handling(json_schema['schema'], path)

    for key, value in json_schema.items():
        if isinstance(value, dict) and 'properties' in value:
            path.append(key)
            legacy_boolean_handling(value['properties'], path)
            path.pop()
        # Update json_schema and save original legacy boolean values
        elif isinstance(value, dict) and 'enum' in value and check_legacy_boolean(value['enum']):
            path = [part.replace('.', '/') for part in path]
            path.append(key)
            current_app.legacy_boolean['/'.join(path)] = {
                translate_legacy_boolean_to_boolean(value['enum'][0]): value['enum'][0],
                translate_legacy_boolean_to_boolean(value['enum'][1]): value['enum'][1]
            }
            value['type'] = 'boolean'
            if 'default' in value:
                value['default'] = translate_legacy_boolean_to_boolean(value['default'])
            value['enum'][0] = translate_legacy_boolean_to_boolean(value['enum'][0])
            value['enum'][1] = translate_legacy_boolean_to_boolean(value['enum'][1])
            path.pop()


def convert_settings_to_legacy_boolean(type=None, subtype=None, settings=None):
    """ Convert boolean settings to legacy boolean (string values) if necessary """
    type = '' if type is None else type
    subtype = '' if subtype is None else subtype
    for key, value in settings.items():
        full_key = type + '/' + subtype + '/' + key
        if full_key in current_app.legacy_boolean:
            settings[key] = current_app.legacy_boolean[full_key][value]

    return settings


def convert_none_to_default_value(settings, default_settings):
    for key, value in settings.items():
        if value is None:
            settings[key] = default_settings[key]
    return settings


def run_command_generator(command, verbose):
    sys.stdout.flush()
    if verbose:
        print('  Command: ')
        print('  ' + command)
        print('  OUTPUT: ')
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    while process.poll() is None or process.stdout.peek(1):
        line = process.stdout.readline().rstrip()
        yield line.decode('utf-8')

    if process.returncode != 0:
        raise subprocess.CalledProcessError(returncode=process.returncode, cmd=command)


def run_command(command, verbose=False, exit_on_error=False, hide_error=False, raise_exception=False):
    silent_output = list()
    command = textwrap.dedent(command)
    try:
        for line in run_command_generator(command, verbose):
            silent_output.append(line)
            if verbose:
                print(line)
    except subprocess.CalledProcessError as e:
        if not hide_error:
            print('FAILED!')
            print('  Command: ')
            print('  ' + command)
            print('  Output: ')
            print('  \n'.join(silent_output))
        if raise_exception:
            raise e
        elif exit_on_error:
            sys.exit(1)
        else:
            return silent_output, e.returncode

    return silent_output, 0


def prefixlen_to_netmask(prefixlen):
    host_bits = 32 - int(prefixlen)
    netmask = socket.inet_ntoa(struct.pack('!I', (1 << 32) - (1 << host_bits)))
    return netmask


def netmask_to_prefixlen(netmask):
    return sum([bin(int(x)).count('1') for x in netmask.split('.')])


def cidr_to_subnetting(cidr):
    network, net_bits = cidr.split('/')
    host_bits = 32 - int(net_bits)
    netmask = socket.inet_ntoa(struct.pack('!I', (1 << 32) - (1 << host_bits)))
    return network, netmask


def subnetting_to_cidr(address, netmask):
    prefixlen = netmask_to_prefixlen(netmask)
    return address + '/' + str(prefixlen)


def is_valid_ipv4_address(address):
    try:
        ipaddress.IPv4Address(address)
    except ipaddress.AddressValueError:  # not a valid address
        return False
    return True


def is_valid_ipv6_address(address):
    try:
        ipaddress.IPv6Address(address)
    except ipaddress.AddressValueError:  # not a valid address
        return False
    return True


def get_boot_arguments(path=None):
    """Reads and parses boot arguments"""
    with open(path or '/proc/cmdline') as src:
        result = dict([(x.split('=', 1) + [True])[:2] for x in src.read().rstrip().split(' ') if x])
    return result


def get_system_log_level():
    boot_arguments = get_boot_arguments()
    if boot_arguments.get('rd.debug', False) or \
            boot_arguments.get('tcos.debug', False) or  \
            boot_arguments.get('LOGGING', 'WARNING') == 'DEBUG':
        return 'DEBUG'
    if boot_arguments.get('rd.info', False) or \
            boot_arguments.get('tcos.info', False) or \
            boot_arguments.get('LOGGING', 'WARNING') == 'INFO':
        return 'INFO'

    return 'WARNING'

def get_mac_addresses():
    command = 'ip -j link show'
    ip_output, exit_code = run_command(command)
    interfaces = json.loads('\n'.join(ip_output))

    return [iface['address'].upper()
            for iface in interfaces
            if (iface['ifname'].startswith(('eth', 'en')) and
                iface['address'] != '00:00:00:00:00:00')]

def get_disk_id():
    command = 'lsblk -Jo NAME,MODEL,VENDOR,SERIAL,MOUNTPOINT'
    lsblk_output, exit_code = run_command(command)
    blockdevices = json.loads(''.join(lsblk_output))['blockdevices']
    for device_info in blockdevices:
        children = device_info.get('children', ())
        while children:
            child = children.pop(0)
            children.extend(child.get('children', ()))
            if child['mountpoint'] == '/run/initramfs/live':
                id_parts = ((device_info.get(key) or '').strip()
                            for key in ('serial', 'model', 'vendor'))
                return ''.join(id_parts)[:255].upper()
