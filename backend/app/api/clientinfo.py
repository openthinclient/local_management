#!/usr/bin/env python

from app import utils
import app.api.schema
import app.api.items
from flask import current_app
import json


def search(mac_address, with_defaults=True, legacy_boolean=True):
	# get database connection
	db = utils.get_db()
	# check if item with it exists
	table_shortitems = db['shortitems']
	if mac_address == '00:00:00:00:00:00':
		item = table_shortitems.find_one(type='client')
	else:
		item = table_shortitems.find_one(mac=mac_address)
	if not item:
		return None, 204

	# Get related location item
	location_items, http_code = app.api.items.search(mac_address=mac_address, type='location', with_defaults=with_defaults)
	if http_code == 200 and len(location_items) > 0:
		location_item = location_items[0]
	else:
		current_app.logger.error('No location assigned to thinclient {mac_address}'.format(**locals()))

	# Get related hardwaretype item
	hardwaretype_items, http_code = app.api.items.search(mac_address=mac_address, type='hardwaretype', with_defaults=with_defaults)
	if http_code == 200 and len(hardwaretype_items) > 0:
		hardwaretype_item = hardwaretype_items[0]
	else:
		current_app.logger.error('No hardwaretype assigned to thinclient {mac_address}'.format(**locals()))


	# get relevant settings table for thinclient
	table_settings = db['settings.client']
	table_location_settings = db['settings.location']
	table_hardwaretype_settings = db ['settings.hardwaretype']

	settings = dict()
	settings['client'] = table_settings.find_one(item_id=item['id'])
	settings['location'] = table_location_settings.find_one(item_id=location_item['id'])
	settings['hardwaretype'] = table_hardwaretype_settings.find_one(item_id=hardwaretype_item['id'])

	default_settings = dict()
	for type in ['client', 'hardwaretype', 'location']:
		current_app.logger.info('Loading default values from JSON Schema'.format(**locals()))
		# Get JSON schema
		response, http_code = app.api.schema.search(type=type)
		json_schema = json.loads(response.get_data(as_text=True))[0]
		# Get default settings
		default_settings[type] = utils.get_defaults(json_schema)

	# Replace None values with their default
	for type in ['client', 'hardwaretype', 'location']:
		settings[type] = utils.convert_none_to_default_value(settings[type], default_settings[type])

	# Load default settings from JSON Schema
	if with_defaults:
		for type in ['client', 'hardwaretype', 'location']:
			# Update settings with default settings
			default_settings[type].update(settings[type])
			settings[type] = default_settings[type]
	else:
		current_app.logger.info('Not loading default values from JSON Schema'.format(**locals()))

	# Convert boolean values to string representations from XML schema
	if legacy_boolean:
		for type in ['client', 'hardwaretype', 'location']:
			settings[type] = utils.convert_settings_to_legacy_boolean(type=type, settings=settings[type])

	# Merge settings
	item['settings'] = settings['hardwaretype']
	item['settings'].update(settings['location'])
	item['settings'].update(settings['client'])

	item['settings'].pop('id', None)
	item['settings'].pop('item_id', None)

	return item, 200
