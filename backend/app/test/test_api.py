import unittest
import json

import app.utils as utils
from app.main import app, application
from flask import current_app


class ApiTestCases(unittest.TestCase):

	item_thinclient = {
		"description": "Thinclient in office1",
		"mac": "00:00:00:11:11:11",
		"name": "TC01",
		"settings": {},
		"subtype": "",
		"type": "thinclient"
	}

	def setUp(self):
		application.config.from_object('app.config.TestingConfig')
		self._clear_database()


	def _clear_database(self):
		with application.app_context():
			db = utils.get_db()
			current_app.logger.debug('Dropping all tables...')
			for table in db.tables:
				db[table].drop()


	# test creating new item successfully
	def test_item_create(self):
		test_client = application.test_client(self)
		response = test_client.post('/v1/item', content_type='application/json',
									data=json.dumps(self.item_thinclient))
		self.assertEqual(response.status_code, 200)
		self.assertTrue('id' in response.json)


	# test updating non existent item
	def test_item_update_non_existent(self):
		test_client = application.test_client(self)
		response = test_client.put('/v1/item?id=1', content_type='application/json',
									data=json.dumps(self.item_thinclient))
		self.assertEqual(response.status_code, 404)


	# test updating item successfully
	def test_item_update(self):
		with application.app_context():
			db = utils.get_db()
			table_shortitems = db['shortitems']
			table_shortitems.insert(dict(type='thinclient', settings=1))
			table_settings = db['settings.thinclient']
			table_settings.insert(dict(item_id=1))

		test_client = application.test_client(self)
		response = test_client.put('/v1/item?id=1', content_type='application/json',
								   data=json.dumps(self.item_thinclient))
		self.assertEqual(response.status_code, 200)

	# test shortitems
	def test_shortitems(self):
		tester = application.test_client(self)
		response = tester.get('/v1/shortitems', content_type='application/json')
		self.assertEqual(response.status_code, 200)


