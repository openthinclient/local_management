from flask import current_app
import locale
import os
import subprocess
import re

WPA_START_LINE = re.compile(r'\S+ \S+ Successfully initialized wpa_supplicant\n')
JOURNAL_ASSOC_TRY = re.compile(r"\S+ \S+ \w+: SME: Trying to authenticate with (\S+) \(SSID='(.*)' .*\)\n")

ENCODING = locale.getpreferredencoding(False) or 'utf-8'

def search():
	result = {}
	ifaces = _run('networkctl', '--no-legend', '--no-pager').stdout
	for line in ifaces.splitlines():
		_, ifname, iftype, _ = line.split(maxsplit=3)
		if iftype == 'wlan':
			result[ifname] = get(ifname)
	return result


def get(ifname):
	if ifname is None:
		return search()

	if ifname is None:
		result = {}
		ifaces = _run('networkctl', '--no-legend', '-no-pager').stdout
		for line in ifaces.splitlines():
			_, ifname, iftype, _ = line.split(3)
			if iftype == 'wlan':
				result[ifname] = get(ifname)
		return result

	# Get tail of wpa_supplicant journal entries for this interface
	journal_result = _run('journalctl',
							f'--unit=wpa_supplicant@{ifname}',
							'-b',
							'--quiet',
							'--no-hostname',
							'--no-pager',
							'--lines=64',
							'--output=short-unix')
	journal_tail = journal_result.stdout

	# If wpa_supplicant just started, throw away lines from previous runs
	journal_tail = WPA_START_LINE.split(journal_tail)[-1]

	# Split into individual connection attempts
	# try_blocks will be an array of this form:
	#     [ MAC, ESSID, 'journal_line\njournal_line\n...',
	#       MAC, ESSID, 'journal_line\njournal_line\n...',
	#       ... ]
	# (Throw away the first element of the split – it contains only data
	# before the first (complete) connection attempt.)
	try_blocks = JOURNAL_ASSOC_TRY.split(journal_tail)[1:]
	if not try_blocks:
		# If the last lines of the journal do not contain a (currently
		# happening) connection attempt, we can trust the result from
		# iwgetid. (Else iwgetid and iwconfig may return a "connected"
		# ESSID before authentication is completed or has failed.)
		return [_get_from_iwgetid(ifname)]

	entries = [_get_from_try_block(*try_blocks[i: i + 3])
					for i in range(0, len(try_blocks), 3) ]

	# remove no longer applicable status and limit to the last 3 results
	return _remove_old_status(entries)[-3:]


def _get_from_try_block(mac, essid, block):
	lines = block.splitlines()

	for i in range(len(lines) - 1, 0, -1):
		message = lines[i].split(maxsplit=2)[2]
		if 'CTRL-EVENT-CONNECTED' in message:
			return {
				'status': 'connected',
				'essid': essid,
				'mac': mac
			}
		if 'CTRL-EVENT-TERMINATING' in message:
			return {
				'status': 'unavailable'
			}
		if 'CTRL-EVENT-SSID-TEMP-DISABLED' in message:
			# Sometimes two consecutive TEMP-DISABLED events are logged.
			# If one of those says WRONG_KEY report as such.
			if ( message.endswith(' reason=WRONG_KEY') or
					(i and lines[i-1].strip().endswith(' reason=WRONG_KEY')) ):
				return {
					'status': 'wrong_key',
					'essid': essid,
					'mac': mac
				}
			else:
				return {
					'status': 'failed',
					'essid': essid,
					'mac': mac
				}
	return {
		'status': 'connecting',
		'essid': essid,
		'mac': mac
	}


def _run(*command):
	return subprocess.run(  command,
							stdout=subprocess.PIPE,
							stderr=subprocess.DEVNULL,
							encoding=ENCODING)


def _get_from_iwgetid(ifname):
	iwgetid_result = _run('iwgetid', ifname, '--raw')
	if iwgetid_result.returncode != 0:
		return {
			'status': 'unavailable'
		}
	essid = iwgetid_result.stdout.strip()
	if (essid):
		return {
			'status': 'connected',
			'essid': essid
		}
	else:
		return {
			'status': 'disconnected'
		}


def _remove_old_status(entries):
	result = []
	for entry in reversed(entries):
		# A general error (i.e. status 'unavailable') has no associated mac
		# and is treated seperately
		if 'mac' not in entry:
			if result:
				continue
			return [entry]
		# Keep status for this connection attempt only if no newer status exists
		if all(entry['mac'] != later_entry['mac'] for later_entry in result):
			result.insert(0, entry)
	return result
