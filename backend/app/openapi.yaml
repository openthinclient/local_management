openapi: "3.0.0"

info:
  version: "1.0"
  title: API for tcos local management
servers:
  - url: /v1
paths:
  /licensing:
    get:
      summary: Get licensing information
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Licensing'
  /license:
    get:
      summary: Get license
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/License'
    post:
      summary: Update or download license
      responses:
        '200':
          description: License updated
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/License'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /license_data:
    get:
      summary: Get license info
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LicenseData'
    post:
      summary: Update license info
      requestBody:
        description: Encryped license
        required: false
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LicenseData'
      responses:
        '200':
          description: License info updated
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /network:
    get:
      summary: Get network config
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NetworkConfig'
        '500':
          description: getting network config failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      summary: Update network config
      requestBody:
        description: json with some/all properties
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NetworkConfig'
      responses:
        '204':
          description: network config updated successfully
        '500':
          description: applying network config failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    delete:
      summary: Delete all manual config
      responses:
        '200':
          description: Removed all manual config
  /wifi_connection:
    get:
      summary: Get list of WiFi cells
      parameters:
        - in: query
          name: ifname
          schema:
            type: string
          required: true
          description: Name of a Wifi interface
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WiFiCells'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /wifi_status:
    get:
      summary: Get connection status of WiFi interfaces
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/WiFiConnectionStatus'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /shortitems:
    get:
      summary: List/filter items
      parameters:
        - in: query
          name: mac_address
          schema:
            type: string
          required: false
          description: Type of item
        - in: query
          name: type
          schema:
            type: string
            enum: ["application","device","printer","location","hardwaretype","client"]
          required: false
          description: Type of item
        - in: query
          name: subtype
          schema:
            type: string
          required: false
          description: Schema of items, e.g. 'citrix', 'freerdp-git', 'display', ...
          example:
            "citrix"
        - in: query
          name: name
          schema:
            type: string
          required: false
          description: Name of item
          example:
            "citrix office"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Shortitem'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /items:
    get:
      summary: List/filter items
      parameters:
        - in: query
          name: mac_address
          schema:
            type: string
          required: false
          description: Type of item
        - in: query
          name: type
          schema:
            type: string
            enum: ["application","device","printer","location","hardwaretype","client"]
          required: false
          description: Type of item
        - in: query
          name: subtype
          schema:
            type: string
          required: false
          description: Schema of items, e.g. 'citrix', 'freerdp-git', 'display', ...
          example:
            "citrix"
        - in: query
          name: name
          schema:
            type: string
          required: false
          description: Name of item
          example:
            "citrix office"
        - in: query
          name: with_defaults
          schema:
            type: boolean
            default: true
          required: false
          description: should the default values be filled in
        - in: query
          name: allow_defaults_only
          schema:
            type: boolean
            default: false
          required: false
          description: should the default values be filled in
        - in: query
          name: legacy_boolean
          schema:
            type: boolean
            default: true
          required: false
          description: convert boolean values to string representations from XML schema
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /clientinfo:
    get:
      summary: Returns full client information including settings from hwtype and location
      parameters:
        - in: query
          name: mac_address
          schema:
            type: string
          required: true
          description: MAC address of client
        - in: query
          name: with_defaults
          schema:
            type: boolean
            default: true
          required: false
          description: should the default values be filled in
        - in: query
          name: legacy_boolean
          schema:
            type: boolean
            default: true
          required: false
          description: convert boolean values to string representations from XML schema
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Item'
        '204':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /item:
    get:
      summary: Get item
      parameters:
        - in: query
          name: id
          schema:
            type: integer
          required: true
          description: ID of item
        - in: query
          name: with_defaults
          schema:
            type: boolean
          required: false
          description: should the default values be filled in
        - in: query
          name: flattened_settings
          schema:
            type: boolean
          required: false
          description: should the settings object be flattened
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Item'
        '204':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      summary: Update item
      parameters:
        - in: query
          name: id
          schema:
            type: integer
          required: true
          description: ID of item
      requestBody:
        description: item with some/all properties
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Item'
      responses:
        '200':
          description: item updated, item id is returned
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
        '204':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      summary: Create new item
      requestBody:
        description: full properties or parts of it (type, schema and name are required!), missing settings will be filled with defaults (MAC Address with 00:00:00:00:00:00)
        required: true
        content:
          application/json:
            schema:
              type: object
      responses:
        '200':
          description: application created, item id is returned
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    delete:
      summary: Delete item
      parameters:
        - in: query
          name: id
          schema:
            type: integer
          required: true
          description: ID of item
      responses:
        '200':
          description: OK
        '404':
          description: item not found
        default:
          description: unexpected error

  /schema:
    get:
      summary: List/filter schemas
      parameters:
        - in: query
          name: type
          schema:
            type: string
            enum: ["application","device","printer","location","hardwaretype","client", ""]
          required: false
          description: Type of items
        - in: query
          name: subtype
          schema:
            type: string
          required: false
          description: Subtype of items, e.g. 'citrix', 'freerdp-git', 'display', ...
          example:
            "citrix"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Schema'

  /relationships:
    get:
      summary: Get direct and indirect relationships
      description: This endpoint resolves direct and indirect relationships. For Thinclients for example it returns
        shortitems for all applications, devices, printers, application groups, hardwaretype and location (direct relationships).
        Furthermore the shortitems for application groups, hardwaretype and location also contain their direct relationsips.
      parameters:
        - in: query
          name: id
          schema:
            type: integer
          required: true
          description: ID of item
        - in: query
          name: flat
          schema:
            type: boolean
            default: false
          required: false
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Relationships'
        '204':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      summary: Update direct relationships
      parameters:
        - in: query
          name: id
          schema:
            type: integer
          required: true
          description: ID of item
      requestBody:
        description: Array of all relationships. Indirect relationships are ignored!!
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Relationships'
      responses:
        '200':
          description: relationship updated, array of direct and indirect relationships is returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Relationships'
        '204':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /file:
    get:
      summary: Download file
      parameters:
        - in: query
          name: type
          schema:
            type: string
          required: true
          description: type of file (e.g. "icon", "certificate")
        - in: query
          name: name
          schema:
            type: string
          required: true
          description: name of file
        - in: query
          name: size
          schema:
            type: string
          required: false
          description: get a thumnail of size f'{width}x{height}'
      responses:
        '200':
          description: OK
          content:
            application/octet-stream:
              schema:
                type: string
                format: binary
        '404':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      summary: Overwrite existing file
      parameters:
        - in: query
          name: type
          schema:
            type: string
          required: true
          description: type of file (e.g. "icon", "certificate")
        - in: query
          name: name
          schema:
            type: string
          required: true
          description: name of file
      requestBody:
        description: file content
        required: true
        content:
          application/octet-stream:
            schema:
              type: string
              format: binary
      responses:
        '204':
          description: file uploaded
        '404':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    delete:
      summary: Delete file
      parameters:
        - in: query
          name: type
          schema:
            type: string
          required: true
          description: type of file (e.g. "icon", "certificate")
        - in: query
          name: name
          schema:
            type: string
          required: true
          description: name of file
      responses:
        '204':
          description: OK
        '404':
          description: item not found
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /files:
    get:
      summary: Download file
      parameters:
        - in: query
          name: type
          schema:
            type: string
          required: true
          description: type of file (e.g. "icon", "certificate")
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /xrandr:
    get:
      summary: Get XRandR information
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      summary: Run xrandr command
      requestBody:
        description: xrandr command
        required: true
        content:
          text/plain:
            schema:
              type: string
              format: text
      responses:
        '204':
          description: command applied
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /command/{command}:
    post:
      summary: Execute command in the OS
      parameters:
        - name: command
          in: path
          schema:
            type: string
            enum: ["desktop-reload", "tcos-configure-wpa"]
          required: true
          description: The command to be run in the OS
      responses:
        '204':
          description: OK
        default:
          description: error from command
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CommandResult'

components:
  schemas:
    CommandResult:
      type: object
      required:
        - code
        - output
      properties:
        code:
          type: integer
          format: int32
        output:
          type: string
    Error:
      type: object
      required:
        - code
        - message
      properties:
        code:
          type: integer
          format: int32
        message:
          type: object
          required:
            - en
            - de
          properties:
            en:
              type: string
            de:
              type: string
    Item:
      type: object
      properties:
        id:
          type: integer
          format: int32
        type:
          type: string
          enum: ["application","device","hardwaretype","location","printer","client"]
        name:
          type: string
        mac:
          type: string
          nullable: true
        subtype:
          type: string
          nullable: true
        description:
          type: string
        settings:
          type: object
      example:
        {
          id: 1,
          type: 'application_group',
          subtype: null,
          name: 'April 1st',
          mac: null,
          description: '',
          settings: {
          }
        }

    Shortitem:
      type: object
      properties:
        id:
          type: integer
          format: int32
        type:
          type: string
          enum: ["application","device","hardwaretype","location","printer","client"]
        mac:
          type: string
          nullable: true
        subtype:
          type: string
        name:
          type: string
        description:
          type: string
      example:
        {
          id: 1,
          type: "application",
          subtype: "cmdline",
          name: "Office Prank",
          mac: null,
          description: ""
        }

    Relationships:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            format: int32
          type:
            type: string
            enum: ["application","device","hardwaretype","location","printer","client"]
          subtype:
            type: string
          name:
            type: string
          description:
            type: string
          relationships:
            type: array
            items:
              $ref: '#/components/schemas/Shortitem'
      example:
        [
          {
            id: 2,
            type: "device",
            subtype: "display",
            mac: null,
            description: "for room 201",
            name: "Dual Display 1688",
          },
          {
            id: 3,
            type: "application_group",
            subtype: null,
            mac: null,
            name: "April 1st",
            description: "",
            relationships:
              {
                id: 1,
                type: "application",
                subtype: "cmdline",
                name: "Office Prank",
                mac: null,
                description: ""
              }
          }
        ]

    Licensing:
      type: object
      example:
        {
          "popupInterval": 1,
          "popupTextEn": "You will be warned one an hour.",
          "popupTextDe": "Sie werden einmal die Stunde gewarnt.",
        }
    LicenseData:
      type: object
      example:
        {
          "customer_id": '30978',
          "pin": "MC68-LF68",
          "mac": "AA:AA:11:11:22:33",
          "email": "m@m.com",
        }
    License:
      type: object
      example:
        {
          "valid": true,
          "customer_id": '30978',
          "pin": "MC68-LF68",
          "mac": "AA:AA:11:11:22:33",
          "email": "m@m.com",
          "expirationDate": "2015-10-26T07:46:36.611000+00:00"
        }
    EncryptedLicense:
      type: object
      example:
        {
          "license": "",
          "encryptionKey": ""
        }
    NetworkConfig:
      type: object
      example:
        {
          "eth0": {
             "type": "ethernet",
             "up": true,
             "carrier": true,
             "DHCP": false,
             "ip": "10.0.0.101",
             "netmask": "255.255.255.0",
             "gateway": "10.0.0.1",
             "nameservers": ["1.1.1.1", "10.0.0.1"]
          }
        }
    WiFiCells:
      type: array
      items:
        type: object
        properties:
          essid:
            type: string
          encryption:
            type: boolean
            default: false
          quality:
            type: array
            items:
              type: integer
      example:
        [
          {
            'encryption': true,
            'essid': 'AP0',
            'quality': ["52", "70"]
          },
          {
            'essid': 'openAP',
            'quality': ["12", "70"]
          }
        ]
    WiFiConnectionStatus:
      type: object
      example:
        {
          "wlxdeadd0d0beef": [
            {
              "status": "disconnected"
            }
          ],
          "wlp2s1": [
            {
              "essid": "Neighbors_WiFi",
              "mac": "01:23:45:67:8a:bc",
              "status": "wrong_key"
            },
            {
              "essid": "MyWiFi",
              "mac": "fe:dc:ba:98:76:54",
              "status": "connecting"
            }
          ]
        }
    Schema:
      type: object
      properties:
        type:
          type: string
        subtype:
          type: string
        schema:
          type: object
        i18n:
          type: object
      example:
        {
          type: "application",
          subtype: "cmdline",
          schema:
          {
            "Application": {
              "type": "object",
              "properties": {
                "cmdline": {
                  "type": "string",
                  "default": ""
                }
              }
            },
            "General": {
              "type": "object",
              "properties": {
                "Autostart": {
                  "type": "boolean",
                  "default": "false"
                },
                "HideIcon": {
                  "type": "string",
                  "enum": ["yes","no","desktop"],
                  "default": "no"
                },
                "custom_icon": {
                  "type": "string",
                  "default": "",
                  "pattern": ".*"
                }
              }
            }
          },
          i18n:
            {
              en: {
                label: "Commandline",
                hint: "",
                "General": {
                  label: "General settings",
                  "Autostart": {
                    label: "Autostart",
                    hint: "",
                  },
                  "HideIcon": {
                    label: "Hide icon",
                    hint: "",
                    enum_labels: {
                        "yes": "Yes",
                        "no": "No",
                        "desktop": "Hide on desktop"
                    }
                  },
                  "custom_icon": {
                    label: "Custom icon",
                    hint: "",
                  },
                },
                "Application": {
                  "Cmdline": {
                    label: "Commandline",
                    hint: "",
                  }
                }
              },
              de: {
                label: "Kommandozeile",
                hint: "",
                "General": {
                  label: "Generelle Einstellungen",
                  "Autostart": {
                    label: "Autostart",
                    hint: "",
                  },
                  "HideIcon": {
                    label: "Verstecke Icon",
                    hint: "",
                    options: {
                      "yes": "Ja",
                      "no": "Nein",
                      "desktop": "Auf Desktop ausblenden"
                    }
                  },
                  "custom_icon": {
                    label: "Benutzerdefiniertes Icon",
                    hint: "",
                  },
                },
                "Application": {
                  "Cmdline": {
                    label: "Kommandozeile",
                    hint: "",
                  }
                }
              },

            }
        }
