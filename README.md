# openthinclient local management


## HACKING & TESTING


### BACKEND

### Requires
  * python >=3.5

#### Setup and start

    cd backend

  * Initialize virtuelenv (optional but recommended)

        export ENV=env
        python3 -m venv $ENV

  * Activate virtualenv for current console session

        . $ENV/bin/activate

  * Install requirements

        pip install --upgrade pip       # just because
        pip install -r requirements.txt

  * Start the server

        # remember to . $ENV/bin/activate your virtualenv first

        export FLASK_ENV=development    # or production
        python3 run.py


### FRONTEND

#### Requires
  * npm / node.js

#### Setup and start

    cd frontend

  * Install requirements (and get coffee)

        npm install

  * Start the (hot-reloading) dev-server

        npm run-script serve --port 1337

  * Optional and pretty: Install vue-cli and use web UI for starting/stopping/monitoring the dev-server

        npm install -g @vue/cli   # global install requires sudo (or similar)
        vue ui


## BUILDING

Get the openthinclient [Makefile] and build as usual.
See .autorun-before-any-target for building the package-rootfs folder.



[Makefile]: https://bitbucket.org/openthinclient/makefile
