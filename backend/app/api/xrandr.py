from app import utils
from flask import current_app
from connexion import NoContent
import re

GEO_RE = re.compile(r'(\d+)x(\d+)\+(\d+)\+(\d+)')
ROT_OPTS = {'left', 'inverted', 'right'}
XRANDR_RE = re.compile( r'^(?:\s*xrandr\s+[- :\w]+;)*\s*xrandr\s+[- :\w]+;*$',
                        flags = re.ASCII | re.IGNORECASE )
CMD_PREFIX = 'export XAUTHORITY=/home/tcos/.Xauthority DISPLAY=:0; '

def search():
    command = 'xrandr -q --current'
    xrandr_output, exit_code = utils.run_command(CMD_PREFIX + command)

    if exit_code != 0:
        xrandr_output = '\n'.join(xrandr_output)
        error = {
            'code': exit_code,
            'message': {
                'de': '"{command}" ist mit folgender Meldung gescheitert: {xrandr_output}'.format(**locals()),
                'en': '"{command}" failed with the following result: {xrandr_output}'.format(**locals()),
            }
        }
        return error, 500

    displays = {}
    current_display = None
    for line in xrandr_output[1:]: # skip virtual screen information
        if not line.strip():
            continue
        try:
            if line.startswith(' '):
                resolution = line.split(maxsplit=1)[0]
                current_display['resolutions'].append([*map(int, resolution.split('x'))])
            else:
                connector_name, *connector_info = line[:line.rindex(' (')].split()
                match = GEO_RE.search(line)
                if match:
                    width, height, *position = match.groups()
                    current_resolution = [int(width), int(height)]
                    rotation = (ROT_OPTS & set(connector_info) or {'normal'}).pop()
                    if rotation in ('left', 'right'):
                        current_resolution.reverse()
                    position = {'x': int(position[0]), 'y': int(position[1])}
                else:
                    current_resolution = []
                    rotation = 'normal'
                    position = {}
                current_display = displays[connector_name] = {
                    'connected': 'connected' in connector_info,
                    'primary': 'primary' in connector_info,
                    'resolution': current_resolution,
                    'rotation': rotation,
                    'position': position,
                    'resolutions': []
                }
        except Exception as ex:
            current_app.logger.error('failed to parse xrandr line:\n{line}'.format(**locals()))
            current_app.logger.exception(ex)

    return displays, 200


def post(body):
    command = body.decode('utf-8')
    if not XRANDR_RE.match(command):
        current_app.logger.info('Illegal xrandr command: {command}'.format(**locals()))
        error = {
            'code': 1,
            'message': {
                'de': '"{command}" ist kein gültiger Aufruf von xrandr'.format(**locals()),
                'en': '"{command}" is not a valid call of xrandr'.format(**locals()),
            }
        }
        return error, 403
    current_app.logger.info('Running xrandr command: {command}'.format(**locals()))
    xrandr_output, exit_code = utils.run_command(CMD_PREFIX + command)
    if exit_code:
        xrandr_output = '\n'.join(xrandr_output)
        error = {
            'code': exit_code,
            'message': {
                'de': '"{command}" ist mit folgender Meldung gescheitert: {xrandr_output}'.format(**locals()),
                'en': '"{command}" failed with the following result: {xrandr_output}'.format(**locals()),
            }
        }
        return error, 500
    else:
        return NoContent, 204
