#!/usr/bin/env python

import os
from flask import render_template, send_from_directory
import connexion
from connexion.resolver import RestyResolver

from app import utils
from flask import current_app

import threading
import requests
import time
import socket


environment = os.getenv('FLASK_ENV', 'production').capitalize()+'Config'

if environment == 'ProductionConfig':
    options = {"swagger_ui": False}
else:
    options = {}

# Create the application instance
app = connexion.App(__name__, specification_dir="./", options=options)

# Read the swagger.yml file to configure the endpoints
app.add_api("openapi.yaml", resolver=RestyResolver('app.api'))

# access flask application directly
application = app.app

# Load config from config module
application.config.from_object('app.config.'+environment)

# Disable CORS restrictions if not in production mode
if application.config['DEBUG']:
    from flask_cors import CORS
    CORS(application, resources={r'/*': {'origins': '*'}})

# create a URL route in our application for "/"
@app.route("/")
def home():
    """
    This function just responds to the browser URL
    localhost:5000/
    :return:        the rendered template "home.html"
    """
    return application.send_static_file("index.html")


@app.route('/<path:path>')
def send_static(path):
    if path.startswith('manager'):
        return application.send_static_file("index.html")
    return send_from_directory('static', path)


@application.before_first_request
def setup():
    # init db
    utils.init_db()
    utils.load_xmls()
    # set log level
    if application.config['DEBUG']:
        current_app.logger.setLevel('DEBUG')
    else:
        current_app.logger.setLevel(utils.get_system_log_level())


def main():
    # run application
    if environment == 'ProductionConfig':
        import bjoern
        bjoern.run(app,
                   host=application.config['HOST'],
                   port=int(application.config['PORT']))
    else:
        app.run(debug=application.config['DEBUG'],
                host=application.config['HOST'],
                port=application.config['PORT'],
                threaded=False)


if __name__ == "__main__":
    main()
