#!/usr/bin/env python


class Config(object):
    DEBUG = False
    TESTING = False

    # Define the application directory
    import os
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    HOST = '0.0.0.0'
    PORT = '5000'


class ProductionConfig(Config):
    DATABASE_URI = "sqlite:////config/db.sqlite"
    XML_PATH = "/tcos/schema"


class DevelopmentConfig(Config):
    DATABASE_URI = "sqlite:///db.sqlite"
    import os
    XML_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "test/schema")
    DEBUG = True


class TestingConfig(Config):
    DATABASE_URI = "sqlite:///:memory:"
    import os
    XML_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "test/schema")
    TESTING = True
    DEBUG = True

file_types = {
    'certificate': {
        'path': '/config/custom/certs',
        'thumbs': '/config/custom/thumbs/certs'
    },
    'icon': {
        'path': '/config/custom/icons',
        'thumbs': '/config/custom/thumbs/icons'
    },
    'wallpaper': {
        'path': '/config/custom/wallpaper',
        'thumbs': '/config/custom/thumbs/wallpaper'
    },
    'openvpn': {
        'path': '/config/custom/openvpn',
        'thumbs': '/config/custom/thumbs/openvpn'
    }
}
