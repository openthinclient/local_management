from app import utils
import re

def search(ifname):
	command = 'ip link set {ifname} up'.format(**locals())
	output_lines, exit_code = utils.run_command(command)
	output = '\n'.join(output_lines)
	if exit_code != 0:
		error = {
			'code': exit_code,
			'message': {
				'de': '"{command}" ist mit folgender Meldung gescheitert: {output_lines}'.format(**locals()),
				'en': '"{command}" failed with the following result: {output_lines}'.format(**locals()),
			}
		}
		return error, 500

	attempts = 0
	while attempts < 3:
		command = 'iwlist {ifname} scan'.format(**locals())
		output_lines, exit_code = utils.run_command(command)
		iwlist_output = '\n'.join(output_lines)
		if exit_code == 0:
			break
	else:
		error = {
			'code': exit_code,
			'message': {
				'de': '"{command}" ist mit folgender Meldung gescheitert: {output_lines}'.format(**locals()),
				'en': '"{command}" failed with the following result: {output_lines}'.format(**locals()),
			}
		}
		return error, 500

	return parse_iwlist(iwlist_output)


CELL_INFO_START = re.compile(r'^ *Cell \d+ *- *', re.MULTILINE)
ADDRESS = re.compile(r'^ *Address: *(\S+)', re.MULTILINE)
ENCRYPTION = re.compile(r'^ *Encryption key: *on', re.MULTILINE)
ESSID = re.compile('^ *ESSID: *"(.*)" *$', re.MULTILINE)
QUALITY = re.compile('^ *Quality= *(\d+)/(\d+)', re.MULTILINE)
ENCRYPTION_INFO = re.compile('''
		\n\s*IE:\ (IEEE\ 802.11i/WPA2\ Version\ 1|WPA\ Version\ 1)
		\s+(.*?)
		(?=\n\s*IE:\ |$)
		''' ,
		re.DOTALL | re.VERBOSE
	)

def parse_iwlist(iwlist_output):
	results = []
	seen_macs = set()
	for cell_info in CELL_INFO_START.split(iwlist_output)[1:]:
		if ENCRYPTION.search(cell_info):
			encryption = True
			if not encryption:
				continue
		else:
			encryption = None

		mac = ADDRESS.search(cell_info).group(1).lower()
		if mac in seen_macs:
			# iwlist soimetimes produces duplicates – simply ignore them
			continue
		seen_macs.add(mac)

		results.append({
			'mac': mac,
			'essid': ESSID.search(cell_info).group(1),
			'quality': QUALITY.search(cell_info).groups(),
			'encryption': encryption
		})
	return results
