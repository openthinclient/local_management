from app import utils
from flask import current_app
from connexion import NoContent
import subprocess

# Endpoint for initiating (tcos-internal) scripts that update the system after
# settings have been written.
# The scripts will request (via tcos_settings) settings from this very same
# single-threaded web server. This can only happen after the current request
# has been processed. So all commands must run in the background and cannot
# return anything.

COMMANDS = {
    'tcos-configure-wpa': "nohup tcos-configure-wpa &",
    # tcos-desktop-populate must be run as user tcos with a correct HOME in env.
    'desktop-reload': 'nohup runuser -l tcos -c "tcos-desktop-populate -r reload --no-autorun" 2&> /dev/null &'
}

def post(command):
    if command not in COMMANDS:
        current_app.logger.error('Unkown command {command}'.format(**locals()))
        return NoContent, 404
    else:
        command = COMMANDS[command]

    current_app.logger.info('Running command: {command}'.format(**locals()))

    subprocess.Popen(command, shell=True)

    return NoContent, 204
